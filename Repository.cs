﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.Serialization.Formatters;
using System.Data;
using System.Runtime.CompilerServices;

namespace HomeWork_6vplanner
{
    struct Repository
    {
        #region Переменные
        private Planner[] planners;

        private string path;

        int index;
        string[] titles;
        #endregion

        #region Репозиторий
        /// <summary>
        /// Репозиторий
        /// </summary>
        /// <param name="Path"></param>
        public Repository(string Path)
        {
            this.path = Path;
            this.index = 0;
            this.titles = new string[5];
            this.planners = new Planner[2];
            this.planners.ToString();
        }
        #endregion

        #region Увеличение размера репозитория
        /// <summary>
        /// Увеличение размера репозитория
        /// </summary>
        /// <param name="Flag"></param>
        private void Resize(bool Flag)
        {
            if (Flag)
            {
                Array.Resize(ref this.planners, this.planners.Length * 3);
            }
        }
        #endregion

        #region Создание записи
        /// <summary>
        /// Создание новой записи
        /// </summary>
        /// <param name="ConcreteWorker"></param>
        public void Add(Planner ConcreteWorker)
        {
            this.Resize(index >= this.planners.Length);
            this.planners[index] = ConcreteWorker;
            this.index++;
        }
        #endregion

        #region Загрузка данных
        /// <summary>
        /// Загрузка данных
        /// </summary>
        public void Load()
        {
            using (StreamReader sr = new StreamReader(this.path))
            {
                titles = sr.ReadLine().Split(',');


                while (!sr.EndOfStream)
                {
                    string[] args = sr.ReadLine().Split(',');

                    Add(new Planner(Convert.ToDateTime(args[0]), args[1], args[2], args[3],args[4],Convert.ToUInt32(args[5]), args[6]));
                }
            }
        }

        public void AddLoad(string path)
        {
            using (StreamReader sr = new StreamReader(path))
            {
                titles = sr.ReadLine().Split(',');


                while (!sr.EndOfStream)
                {
                    string[] args = sr.ReadLine().Split(',');

                    Add(new Planner(Convert.ToDateTime(args[0]), args[1], args[2], args[3], args[4], Convert.ToUInt32(args[5]), args[6]));
                }
            }
        }
        #endregion

        #region Печать репозитория
        /// <summary>
        /// Печать репозитория в консоль
        /// </summary>
        public void PrintToConsole()
        {
            Console.WriteLine($"{this.titles[0],15} {this.titles[1],15} {this.titles[4],15} {this.titles[2],15} {this.titles[3],10}");

            for (int i = index - 1; i >= 0; i--)//Сортировка по дате
            {
                for (int j = 0; j < i; j++)
                {
                    if (this.planners[j].Time > this.planners[j + 1].Time)
                    {
                        Planner tmp = this.planners[j];
                        this.planners[j] = this.planners[j + 1];
                        this.planners[j + 1] = tmp;
                    }
                }
            }

            for (int i = 0; i < index; i++)
            {
                if (i == 0)
                {
                    Console.WriteLine($"{i + 1} {this.planners[i].Print()}");
                    Console.WriteLine();
                }
                else
                {
                    if (this.planners[i].Time.DayOfYear != this.planners[i - 1].Time.DayOfYear)
                    {
                        Console.WriteLine($"{i + 1} {this.planners[i].Print()}");
                        Console.WriteLine();
                    }
                    else
                    {
                        if (this.planners[i].Time.DayOfYear == this.planners[i - 1].Time.DayOfYear)
                        {
                            Console.WriteLine($"{i + 1} {this.planners[i].Print1()}");
                            Console.WriteLine();
                        }
                    }
                }
            }
            Console.WriteLine($"Количество записи в ежедневнике {Count}");
        }
        #endregion

        #region Удаление записи ежедневника
        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <param name="del"></param>
        public void Delete(int del)
        {
            Console.WriteLine($"\nУдаление записи под № {del+1}");
            for (int i = 0;i<index;i++)
            {
                if(i==del)
                {
                    this.planners[del] = this.planners[i+1];
                }
                if(i>del)
                {
                    this.planners[i] = this.planners[i + 1];
                }
                if (i == index - 1)
                {
                    index--;
                }
            }
        }
        #endregion

        #region Изменение записи ежедневника
        /// <summary>
        /// Изменение записи ежедневника
        /// </summary>
        /// <param name="edit"></param>
        /// <param name="time"></param>
        /// <param name="work"></param>
        /// <param name="comment"></param>
        /// <param name="place"></param>
        /// <param name="namecontact"></param>
        /// <param name="numbercontact"></param>
        /// <param name="status"></param>
        public void Edit(int edit,DateTime time, string work, string comment, string place, string namecontact, long numbercontact, string status)
        {
            Console.WriteLine($"\nРедактирование записи под № {edit + 1}");
            for (int i = 0; i < index; i++)
            {
                if (i == edit)
                {
                    this.planners[i].Time = time;
                    this.planners[i].Work = work;
                    this.planners[i].Comment = comment;
                    this.planners[i].Place = place;
                    this.planners[i].NameContact = namecontact;
                    this.planners[i].NumberContact = numbercontact;
                    this.planners[i].Status = status;
                }
            }
        }
        #endregion

        #region Импорт записей по выбранному диапозону дат
        /// <summary>
        /// Импорт записей по выбранному диапозону дат
        /// </summary>
        /// <param name="timebegin"></param>
        /// <param name="timeend"></param>
        public void Datesearch(DateTime timebegin, DateTime timeend)
        {
            int count = 0;
            Console.WriteLine($"\nИмпорт дат с {timebegin} до {timeend}");
            Console.WriteLine();
            for(int i = 0;i<index;i++)
            {

                if (this.planners[i].Time > timebegin&&this.planners[i].Time<timeend)
                {
                    if (count == 0)
                    {
                        Console.WriteLine($"{i + 1} {this.planners[i].Print()}");
                        Console.WriteLine();
                        if(this.planners[i].Time.DayOfYear == this.planners[i + 1].Time.DayOfYear)
                        {
                            count++;
                        }
                        else
                        {
                            if (this.planners[i].Time.DayOfYear != this.planners[i + 1].Time.DayOfYear)
                            {
                                count = 0;
                            }
                        }
                    }
                    else
                    { 
                        if(count > 0)
                        {
                            if(this.planners[i].Time.DayOfYear == this.planners[i + 1].Time.DayOfYear)
                            {
                                Console.WriteLine($"{i + 1} {this.planners[i].Print1()}");
                                Console.WriteLine();
                            }
                            else
                            {
                                if (this.planners[i].Time.DayOfYear != this.planners[i + 1].Time.DayOfYear)
                                {
                                    Console.WriteLine($"{i + 1} {this.planners[i].Print1()}");
                                    Console.WriteLine();
                                    count = 0;
                                }
                            }
                        }
                    }
                }
            }
            Console.WriteLine($"Количество записи в ежедневнике {Count}");
        }
        #endregion

        #region Количество записей в ежедневнике
        /// <summary>
        /// Количество дел в ежедневнике
        /// </summary>
        public int Count { get { return this.index; } }
        #endregion

        #region Сортировки

        #region Сортировка в консольном меню
        public void Sort()
        {
            int i = 0;
            int m = planners.Length;
            int n = m - index;
            Console.WriteLine("\nВыберите как Вы желаете сортировать ежедневник:");
            Console.WriteLine("Если Вы желаете сортировать по дате - нажмите 1");
            Console.WriteLine("Если Вы желаете сортировать по делу - нажмите 2");
            Console.WriteLine("Если Вы желаете сортировать по комментарию - нажмите 3");
            Console.WriteLine("Если Вы желаете сортировать по месту - нажмите 4");
            Console.WriteLine("Если Вы желаете сортировать по имени контакта - нажмите 5");
            Console.WriteLine("Если Вы желаете сортировать по номеру контакта - нажмите 6");
            Console.WriteLine("Если Вы желаете сортировать по статусу- нажмите 7");
            int N = int.Parse(Console.ReadLine());

            IEnumerable<Planner> v = null;
            switch (N)
            {
                case 1: Console.WriteLine("Сортировка по времени!\n"); v = this.planners.OrderBy(q => q.Time); break;
                case 2: Console.WriteLine("Сортировка по делу!\n"); v = this.planners.OrderBy(q => q.Work); break;
                case 3: Console.WriteLine("Сортировка по комментарию!\n"); v = this.planners.OrderBy(q => q.Comment); break;
                case 4: Console.WriteLine("Сортировка по месту!\n"); v = this.planners.OrderBy(q => q.Place); break;
                case 5: Console.WriteLine("Сортировка по имени контакта!\n"); v = this.planners.OrderBy(q => q.NameContact); break;
                case 6: Console.WriteLine("Сортировка по номеру контакта!\n"); v = this.planners.OrderBy(q => q.NumberContact); break;
                case 7: Console.WriteLine("Сортировка по статусу!\n"); v = this.planners.OrderBy(q => q.status); break;
                default: Console.WriteLine("Такой сортировки нет! Осуществим сортировку по времени!\n"); v = this.planners.OrderBy(q => q.Time); break;
            }
            foreach (Planner planner in v)
            {
                if (i < n+1) { i++; }
                if (i > n && i < index + (n+1))
                {
                    Console.WriteLine($"{i-n} Дата: {planner.Time.ToShortDateString()} День недели: {planner.DayofWeek()} " +
                    $"Время: {planner.Time.ToShortTimeString()}   Дело: {planner.Work} " +
                    $"  Коменатрий: {planner.Comment}  Место: {planner.Place}  Контакт: {planner.NameContact} " +
                    $"  Номер контакта: {planner.NumberContact}  Статус: {planner.Status}");
                    Console.WriteLine();
                    i++;
                }
            }
            Console.WriteLine($"Количество записи в ежедневнике {Count}");
        }
        #endregion

        #region Сортировка
        public void Sort(int N)
        {
            int i = 0;
            int m = planners.Length;
            int n = m - index;
            IEnumerable<Planner> v = null;
            switch (N)
            {
                case 1: Console.WriteLine("\nСортировка по времени!\n"); v = this.planners.OrderBy(q => q.Time); break;
                case 2: Console.WriteLine("Сортировка по делу!\n"); v = this.planners.OrderBy(q => q.Work); break;
                case 3: Console.WriteLine("Сортировка по комментарию!\n"); v = this.planners.OrderBy(q => q.Comment); break;
                case 4: Console.WriteLine("Сортировка по месту!\n"); v = this.planners.OrderBy(q => q.Place); break;
                case 5: Console.WriteLine("Сортировка по имени контакта!\n"); v = this.planners.OrderBy(q => q.NameContact); break;
                case 6: Console.WriteLine("Сортировка по номеру контакта!\n"); v = this.planners.OrderBy(q => q.NumberContact); break;
                case 7: Console.WriteLine("Сортировка по статусу!\n"); v = this.planners.OrderBy(q => q.status); break;
                default: Console.WriteLine("Такой сортировки нет! Осуществим сортировку по времени!\n"); v = this.planners.OrderBy(q => q.Time); break;
            }
            foreach (Planner planner in v)
            {
                if (i < n + 1) { i++; }
                if (i > n && i < index + (n + 1))
                {
                    Console.WriteLine($"{i - n}   Дата: {planner.Time.ToShortDateString()} День недели: {planner.DayofWeek()} " +
                    $"Время: {planner.Time.ToShortTimeString()}   Дело: {planner.Work} " +
                    $"  Коменатрий: {planner.Comment}  Место: {planner.Place}  Контакт: {planner.NameContact} " +
                    $"  Номер контакта: {planner.NumberContact}  Статус: {planner.Status}");
                    Console.WriteLine();
                    i++;
                }
            }
            Console.WriteLine($"Количество записи в ежедневнике {Count}");
        }
    
        #endregion

        #endregion

        #region Выгрузка
        /// <summary>
        /// Выгрузка
        /// </summary>
        /// <param name="path"></param>
        public void Save(string path)
        {
            using (StreamWriter sr = new StreamWriter(this.path))
            {
                foreach(var item in planners)
                {
                    sr.WriteLine(item.ToString());
                }
            }
        }
        #endregion
    }
}
 