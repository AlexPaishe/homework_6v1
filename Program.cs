﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Reflection;
using System.Reflection.Metadata;


namespace HomeWork_6vplanner
{
    class Program
    {

        static void Main(string[] args)
        {
            //string path = @"HomeWork_6v5.csproj";
            string path = @"docs.txt";
            Repository repos = new Repository(path);
            //repos.Load();
            repos.PrintToConsole();
            repos.Add(new Planner(new DateTime(2020, 03, 19, 07, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Юля Андреева", 89104578234, "-"));
            repos.Add(new Planner(new DateTime(2020, 03, 19, 07, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Юля Андреева", 89104578234, "+"));
            repos.Add(new Planner(new DateTime(2020, 03, 19, 08, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Юля Андреева", 89104578234, "?"));
            repos.Add(new Planner(new DateTime(2020, 03, 20, 00, 30, 00), "встреча", "получить удовольствие от общения", "Ресторан Море Суши", "Маша Себастьянская", 89104523234, "!"));
            repos.Add(new Planner(new DateTime(2020, 03, 20, 07, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Юля Андреева", 89104578234, "-"));
            repos.Add(new Planner(new DateTime(2020, 03, 20, 08, 30, 00), "Пробежка", "6 кругов", "ВДНХ", "Мария Летучева", 89104753123, "-"));
            repos.Add(new Planner(new DateTime(2020, 03, 20, 09, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Юлия Воронина", 89854578234, "-"));
            repos.Add(new Planner(new DateTime(2020, 03, 21, 07, 30, 00), "Пробежка", "6 кругов", "Парк Победы", "Юля Андреева", 89104578234, "-"));
            repos.Add(new Planner(new DateTime(2020, 03, 22, 07, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Юлия Воронина", 89854578234, "!"));
            repos.Add(new Planner(new DateTime(2020, 03, 22, 10, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Наташа Устинова", 89184578234, "-"));
            repos.PrintToConsole();
            repos.Delete(5);
            repos.PrintToConsole();
            repos.Edit(0, new DateTime(2020, 03, 15, 07, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Юля Андреева", 89104578234, "!");
            repos.PrintToConsole();
            repos.Datesearch(new DateTime(2020, 03, 20), new DateTime(2020, 03, 23));
            repos.Add(new Planner(new DateTime(2020, 03, 16, 10, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Оля Малая", 89104545234, "?"));
            repos.Add(new Planner(new DateTime(2020, 03, 28, 10, 30, 00), "Пробежка", "6 кругов", "Парк Горького", "Оля Малая", 89104545234, "!"));
            repos.PrintToConsole();
            repos.Sort(7);
            repos.Sort();
            Type myType = Type.GetType("repos", false, true);
        }
    }
}