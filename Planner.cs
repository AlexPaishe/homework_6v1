﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork_6vplanner
{
    struct Planner
    {   
        #region Дата и Время
        /// <summary>
        /// Дата и Время
        /// </summary>
        public DateTime Time;
        #endregion

        #region Дело
        /// <summary>
        /// Дело
        /// </summary>
        public string Work;
        #endregion

        #region Комментарии к делу
        /// <summary>
        /// Комментарии к делу
        /// </summary>
        public string Comment;
        #endregion

        #region Место действия
        /// <summary>
        /// Место действия
        /// </summary>
        public string Place;
        #endregion

        #region Имя контакта
        /// <summary>
        /// Имя контакта
        /// </summary>
        public string  NameContact;
        #endregion

        #region Номер контакта
        /// <summary>
        /// Номер Контакта
        /// </summary>
        public long NumberContact;
        #endregion

        #region Статус
        /// <summary>
        /// Статус
        /// </summary>
        public string status;

        /// <summary>
        /// Чтение статуса и редактирование статуса
        /// </summary>
        public string Status
        {
            get
            {
                switch (status)
                {
                    case "!": return "Важно";
                    case "+": return "Выполнено";
                    case "-": return "Невыполнено";
                    default: return "Неважно";
                }
            }
            set { this.status = value; }
        }
        #endregion
       
        #region Печать

        #region Отступ
        /// <summary>
        /// Отступ
        /// </summary>
        /// <returns></returns>
        public string Space()
        {
            return "                                          ";
        }
        #endregion

        #region День недели
        /// <summary>
        /// Перевод дня недели с английского на русский
        /// </summary>
        /// <returns></returns>
        public string DayofWeek()
        {
            switch(Time.DayOfWeek)
            {
                case DayOfWeek.Monday: return "Понедельник";
                case DayOfWeek.Tuesday: return "  Вторник  ";
                case DayOfWeek.Wednesday: return "   Среда   ";
                case DayOfWeek.Thursday: return "  Четверг  ";
                case DayOfWeek.Friday: return "  Пятница  ";
                case DayOfWeek.Saturday: return "  Суббота  ";
                case DayOfWeek.Sunday: return "Воскресенье";
                default: return "Не известный день недели";
            }
        }
        #endregion

        #region Печать Структуры с Датой
        /// <summary>
        /// Печать структуры с Датой
        /// </summary>
        /// <returns></returns>
        public string Print()
        {
            return $" Дата: {Time.ToShortDateString(),10} День недели: {DayofWeek(),11} Время: {Time.ToShortTimeString()}   Дело: {Work} " +
                $"  Коменатрий: {Comment}  Место: {Place}  Контакт: {NameContact} " +
                $"  Номер контакта: {NumberContact}  Статус: {Status}";
        }
        #endregion

        #region Печать Структуры без Даты
        /// <summary>
        /// Печать структуры с Датой
        /// </summary>
        /// <returns></returns>
        public string Print1()
        {
            return $"{Space()} Время: {Time.ToShortTimeString()}   Дело: {Work} " +
                $"  Коменатрий: {Comment}  Место: {Place}  Контакт: {NameContact} " +
                $"  Номер контакта: {NumberContact}  Статус: {Status}";
        }
        #endregion

        #endregion

        #region Конструкторы
        /// <summary>
        /// Полная структура
        /// </summary>
        /// <param name="Time"></param>
        /// <param name="Work"></param>
        /// <param name="Comment"></param>
        /// <param name="Place"></param>
        /// <param name="NameContact"></param>
        /// <param name="NumberContact"></param>
        /// <param name="Status"></param>
        public Planner(DateTime Time, string Work, string Comment, string Place, string NameContact, long NumberContact,string Status)
        {
            this.Time = Time;
            this.Work = Work;
            this.Comment = Comment;
            this.Place = Place;
            this.NameContact = NameContact;
            this.NumberContact = NumberContact;
            this.status = Status;
        }

        /// <summary>
        /// Не полная структура - отсутствует контакт и номер контакта
        /// </summary>
        /// <param name="Time"></param>
        /// <param name="Work"></param>
        /// <param name="Comment"></param>
        /// <param name="Place"></param>
        /// <param name="Status"></param>
        public Planner(DateTime Time, string Work, string Comment, string Place, string Status) :
            this(Time, Work, Comment, Place, "Нет", 0, Status)
        {

        }
        #endregion
    }   
}
